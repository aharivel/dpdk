/* SPDX-License-Identifier: BSD-3-Clause
 * Copyright 2017 Mellanox Technologies, Ltd
 */

#define MAX_PATTERN_NUM		4
#define MAX_ACTION_NUM		3

struct rte_flow *
generate_bfd_ipv4_flow(uint16_t port_id, uint16_t rx_q,
		struct rte_flow_error *error);

struct rte_flow *
generate_bfd_ipv6_flow(uint16_t port_id, uint16_t rx_q,
		struct rte_flow_error *error);

struct rte_flow *
generate_bfd_ovs_flow(uint16_t port_id, uint16_t rx_q,
		struct rte_flow_error *error);

#define BFD_DEST_PORT	3784
#define BFD_SRC_PORT	49152
#define ETH_TYPE_IP	0x0800

/**
 * create a BFD ipv4 flow rule for OVS
 *
 * @param port_id
 *   The selected port.
 * @param rx_q
 *   The selected target queue.
 * @param[out] error
 *   Perform verbose error reporting if not NULL.
 *
 * @return
 *   A flow if the rule could be created else return NULL.
 */
struct rte_flow *
generate_bfd_ovs_flow(uint16_t port_id, uint16_t rx_q,
		struct rte_flow_error *error)
{
	/* Declaring structs being used. 8< */
	struct rte_flow_attr attr;
	struct rte_flow_action action[MAX_ACTION_NUM];
	struct rte_flow *flow = NULL;
	struct rte_flow_action_queue queue = { .index = rx_q };
	/* >8 End of declaring structs being used. */
	int res;

	memset(action, 0, sizeof(action));

	/* Set the rule attribute, only ingress packets will be checked. 8< */
	memset(&attr, 0, sizeof(struct rte_flow_attr));
	attr.ingress = 1;
	/* >8 End of setting the rule attribute. */

	/*
	 * create the action sequence.
	 * - count
	 * - move packet to queue
	 */
	action[0].type = RTE_FLOW_ACTION_TYPE_QUEUE;
	action[0].conf = &queue;
	action[1].type = RTE_FLOW_ACTION_TYPE_END;


        const struct rte_flow_item items_ipv4[] = {
        {
            .type = RTE_FLOW_ITEM_TYPE_ETH,
            .spec = &(const struct rte_flow_item_eth){
                .type = htons(ETH_TYPE_IP),
            },
            .mask = &(const struct rte_flow_item_eth){
                .type = htons(0xffff),
            },
        },
        {
            .type = RTE_FLOW_ITEM_TYPE_IPV4,
            .spec = &(const struct rte_flow_item_ipv4){
                .hdr = {
                    .dst_addr = rte_cpu_to_be_32(BFD_DEST_PORT),
                    .src_addr = rte_cpu_to_be_32(BFD_SRC_PORT),
                    .next_proto_id = IPPROTO_UDP,
                },
            },
            .mask = &(const struct rte_flow_item_ipv4){
                .hdr = {
                    .dst_addr = RTE_BE32(0xffffffff),
                    .src_addr = RTE_BE32(0xffffffff),
                    .next_proto_id = 0xff,
                },
            },
        },
        {
            .type = RTE_FLOW_ITEM_TYPE_UDP,
            .spec = &(const struct rte_flow_item_udp){
                .hdr = {
                    .dst_port = rte_cpu_to_be_16(BFD_DEST_PORT),
                    .src_port = rte_cpu_to_be_16(BFD_SRC_PORT),
                },
            },
            .mask = &(const struct rte_flow_item_udp){
                .hdr = {
                    .dst_port = RTE_BE16(0xffff),
                    .src_port = RTE_BE16(0xffff),
                },
            },
        },
        { .type = RTE_FLOW_ITEM_TYPE_END },
        };

	/* Validate the rule and create it. 8< */
	res = rte_flow_validate(port_id, &attr, items_ipv4, action, error);
	if (!res)
		flow = rte_flow_create(port_id, &attr, items_ipv4, action, error);
	/* >8 End of validation the rule and create it. */

	return flow;
}

/**
 * create a ipv4 flow rule that sends BFD packets to the selected queue.
 *
 * @param port_id
 *   The selected port.
 * @param rx_q
 *   The selected target queue.
 * @param[out] error
 *   Perform verbose error reporting if not NULL.
 *
 * @return
 *   A flow if the rule could be created else return NULL.
 */

/* Function responsible for creating the flow rule. 8< */
struct rte_flow *
generate_bfd_ipv4_flow(uint16_t port_id, uint16_t rx_q,
		struct rte_flow_error *error)
{
	/* Declaring structs being used. 8< */
	struct rte_flow_attr attr;
	struct rte_flow_item pattern[MAX_PATTERN_NUM];
	struct rte_flow_action action[MAX_ACTION_NUM];
	struct rte_flow *flow = NULL;
	struct rte_flow_action_queue queue = { .index = rx_q };
	struct rte_flow_item_udp udp_spec;
	struct rte_flow_item_udp udp_mask;
	/* >8 End of declaring structs being used. */
	int res;

	memset(pattern, 0, sizeof(pattern));
	memset(action, 0, sizeof(action));

	/* Set the rule attribute, only ingress packets will be checked. 8< */
	memset(&attr, 0, sizeof(struct rte_flow_attr));
	attr.ingress = 1;
	/* >8 End of setting the rule attribute. */

	/*
	 * create the action sequence.
	 * - count
	 * - move packet to queue
	 */
	action[0].type = RTE_FLOW_ACTION_TYPE_QUEUE;
	action[0].conf = &queue;
	action[1].type = RTE_FLOW_ACTION_TYPE_END;

	/*
	 * set the first level of the pattern (ETH).
	 * since in this example we just want to get the
	 * ipv4 we set this level to allow all.
	 */

	/* Set this level to allow all. 8< */
	pattern[0].type = RTE_FLOW_ITEM_TYPE_ETH;
	/* >8 End of setting the first level of the pattern. */

	/*
	 * setting the second level of the pattern (IP).
	 * in this example this is the level we care about
	 * so we set it to allow all IPv4 packets.
	 */

	/* Setting the second level of the pattern. 8< */
	pattern[1].type = RTE_FLOW_ITEM_TYPE_IPV4;
	pattern[1].spec = NULL; // No specific IP address matching
	pattern[1].mask = NULL; // Allow all IPv4 packets
	/* >8 End of setting the second level of the pattern. */

	/*
	 * setting the third level of the pattern (UDP).
	 * match UDP packets with destination port 3784 or 3785.
	 */

	/* Setting the third level of the pattern. 8< */
	memset(&udp_spec, 0, sizeof(struct rte_flow_item_udp));
	memset(&udp_mask, 0, sizeof(struct rte_flow_item_udp));
	udp_spec.hdr.dst_port = rte_cpu_to_be_16(3784); // Match UDP port 3784
	udp_mask.hdr.dst_port = 0xFFFF; // Exact match on port
	pattern[2].type = RTE_FLOW_ITEM_TYPE_UDP;
	pattern[2].spec = &udp_spec;
	pattern[2].mask = &udp_mask;
	/* >8 End of setting the third level of the pattern. */

	/* The final level must be always type end. 8< */
	pattern[3].type = RTE_FLOW_ITEM_TYPE_END;
	/* >8 End of final level must be always type end. */

	/* Validate the rule and create it. 8< */
	res = rte_flow_validate(port_id, &attr, pattern, action, error);
	if (!res)
		flow = rte_flow_create(port_id, &attr, pattern, action, error);
	/* >8 End of validation the rule and create it. */

	return flow;
}

/**
 * create a ipv6 flow rule that sends BFD packets to the selected queue.
 *
 * @param port_id
 *   The selected port.
 * @param rx_q
 *   The selected target queue.
 * @param[out] error
 *   Perform verbose error reporting if not NULL.
 *
 * @return
 *   A flow if the rule could be created else return NULL.
 */

/* Function responsible for creating the flow rule. 8< */
struct rte_flow *
generate_bfd_ipv6_flow(uint16_t port_id, uint16_t rx_q,
		struct rte_flow_error *error)
{
	/* Declaring structs being used. 8< */
	struct rte_flow_attr attr;
	struct rte_flow_item pattern[MAX_PATTERN_NUM];
	struct rte_flow_action action[MAX_ACTION_NUM];
	struct rte_flow *flow = NULL;
	struct rte_flow_action_queue queue = { .index = rx_q };
	struct rte_flow_item_udp udp_spec;
	struct rte_flow_item_udp udp_mask;
	/* >8 End of declaring structs being used. */
	int res;

	memset(pattern, 0, sizeof(pattern));
	memset(action, 0, sizeof(action));

	/* Set the rule attribute, only ingress packets will be checked. 8< */
	memset(&attr, 0, sizeof(struct rte_flow_attr));
	attr.ingress = 1;
	/* >8 End of setting the rule attribute. */

	/*
	 * create the action sequence.
	 * - count
	 * - move packet to queue
	 */
	action[0].type = RTE_FLOW_ACTION_TYPE_QUEUE;
	action[0].conf = &queue;
	action[1].type = RTE_FLOW_ACTION_TYPE_END;

	/*
	 * set the first level of the pattern (ETH).
	 * since in this example we just want to get the
	 * ipv4 we set this level to allow all.
	 */

	/* Set this level to allow all. 8< */
	pattern[0].type = RTE_FLOW_ITEM_TYPE_ETH;
	/* >8 End of setting the first level of the pattern. */

	/*
	 * setting the second level of the pattern (IP).
	 * in this example this is the level we care about
	 * so we set it to allow all IPv4 packets.
	 */

	/* Setting the second level of the pattern. 8< */
	pattern[1].type = RTE_FLOW_ITEM_TYPE_IPV6;
	pattern[1].spec = NULL; // No specific IP address matching
	pattern[1].mask = NULL; // Allow all IPv6 packets
	/* >8 End of setting the second level of the pattern. */

	/*
	 * setting the third level of the pattern (UDP).
	 * match UDP packets with destination port 3784 or 3785.
	 */

	/* Setting the third level of the pattern. 8< */
	memset(&udp_spec, 0, sizeof(struct rte_flow_item_udp));
	memset(&udp_mask, 0, sizeof(struct rte_flow_item_udp));
	udp_spec.hdr.dst_port = rte_cpu_to_be_16(3784); // Match UDP port 3784
	udp_mask.hdr.dst_port = 0xFFFF; // Exact match on port
	pattern[2].type = RTE_FLOW_ITEM_TYPE_UDP;
	pattern[2].spec = &udp_spec;
	pattern[2].mask = &udp_mask;
	/* >8 End of setting the third level of the pattern. */

	/* The final level must be always type end. 8< */
	pattern[3].type = RTE_FLOW_ITEM_TYPE_END;
	/* >8 End of final level must be always type end. */

	/* Validate the rule and create it. 8< */
	res = rte_flow_validate(port_id, &attr, pattern, action, error);
	if (!res)
		flow = rte_flow_create(port_id, &attr, pattern, action, error);
	/* >8 End of validation the rule and create it. */

	return flow;
}
/* >8 End of function responsible for creating the flow rule. */

/* >8 End of function responsible for creating the flow rule. */

